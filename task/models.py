from __future__ import unicode_literals

from django.db import models

# Create your models here.
# Here our model where we add new subjects
class PemSubject(models.Model):
	C = models.CharField(max_length=200, blank=True, null=True, verbose_name='Country')
	ST = models.CharField(max_length=200, blank=True, null=True, verbose_name='State')
	L = models.CharField(max_length=200, blank=True, null=True, verbose_name='Location')
	O = models.CharField(max_length=200, blank=True, null=True, verbose_name='Organization')
	OU = models.CharField(max_length=200, blank=True, null=True, verbose_name='Organization Unit')
	CN = models.CharField(max_length=200, blank=True, null=True, verbose_name='Common Name')
	emailAddress = models.CharField(max_length=200, blank=True, null=True, verbose_name='Email Address')

	def __unicode__(self):
		return self.organization

