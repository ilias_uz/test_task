from django.test import TestCase
from task.models import PemSubject
import OpenSSL.crypto as pem_decoder
# Create your tests here.


class PemSubjectTestCase(TestCase):
	
	def test_add_subject_and_update(self):
		# Get our certificates and add them in database
		st_cert=open('task/tests/cert.pem', 'rt').read()
		c=pem_decoder
		cert=c.load_certificate(c.FILETYPE_PEM, st_cert)
		subject = cert.get_subject()

		# Create new subject after parsing from certificate
		new_subject = PemSubject.objects.create(**dict(subject.get_components()))
		new_subject.save()
		self.assertEqual(new_subject.C, 'KG')

		#Update exist subject
		PemSubject.objects.filter(pk=1).update(C='RU', ST='NS', O='ASH')
		get_subject = PemSubject.objects.get(pk=1)
		self.assertEqual(get_subject.C, 'RU')




