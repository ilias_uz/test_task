# -*- coding: utf-8 -*-
from django.shortcuts import render, redirect
import OpenSSL.crypto as cert_decoder
import sqlite3
from models import PemSubject
from django.db import connection

# I was use some times objects.raw for clean SQL and some time django connection for connet to database

# main page
def index(request):
	return render(request, 'index.html')


# This function return templates for add certificate
def add_certificate_sql(request):
	return render(request, 'add_certificate_sql.html')


# Here the function wich do parsing certificate and get subject from certificate after that it will be add to database.
def create_subject_sql(request):
	pem_text = request.FILES['pem_file']
	open('cert.pem', 'wb').write(pem_text.read())

	# Parsing subject
	st_cert=open('cert.pem', 'rt').read()
	c=cert_decoder
	cert=c.load_certificate(c.FILETYPE_PEM, st_cert)
	subject = cert.get_subject()

	# Create new PemSubject
	pem_subject = PemSubject.objects.raw("SELECT * FROM task_pemsubject ORDER BY id DESC LIMIT 1")
	last_id = 0
	try:
		last_id = pem_subject[0].id
	except IndexError:
		pass
	# Here we are connecting to the database for add new subject.
	with connection.cursor() as cursor:
		cursor.execute("INSERT INTO task_pemsubject (id,C,ST,L,O,OU,CN,emailAddress) VALUES(%s,%s,%s,%s,%s,%s,%s,%s)", 
						(last_id+1,subject.C, subject.ST, subject.L, subject.O, subject.OU, 
							subject.CN, subject.emailAddress))
	return redirect('/subject_list_sql')


# This is the update function for subject table
def update_subject_sql(request, id):
	# Get exist data from db
	pem_subject = PemSubject.objects.raw("SELECT * FROM task_pemsubject WHERE id=%s" % id)
	
	# If request method is POST we get the datas with changes and add updated data to the database 
	if request.method == 'POST':
		country = request.POST.get('country')
		state = request.POST.get('state')
		location = request.POST.get('location')
		organization = request.POST.get('organization')
		organization_unit = request.POST.get('organization_unit')
		common_name = request.POST.get('common_name')
		email_address = request.POST.get('email_address')
		
		# Here we are connecting to the database for update some information.
		with connection.cursor() as cursor:
			cursor.execute("UPDATE task_pemsubject SET C=%s, ST=%s, L=%s, O=%s, OU=%s, CN=%s, emailAddress=%s WHERE id=%s", 
							(country,state,location, organization, organization_unit,common_name,email_address,id))
		
		# If all is ok will return subject list with some changes
		return redirect('/subject_list_sql')

	return render(request, 'edit_sub.html', {'pem_subject': pem_subject})


#And this function will help us to delete one of the subject from database
def delete_subject_sql(request, id):
	with connection.cursor() as cursor:
		cursor.execute("DELETE FROM task_pemsubject WHERE id=%s", (id,))
	return redirect('/subject_list_sql')


# Last one function get all subjects from database and will show on template
def subject_list_sql(request):
	pem_subject = PemSubject.objects.raw("SELECT * FROM task_pemsubject")
	return render(request, 'sub_list.html', {'pem_subject': pem_subject})

