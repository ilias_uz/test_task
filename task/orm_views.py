# -*- coding: utf-8 -*-
from django.shortcuts import render, redirect
from django.views.generic.edit import UpdateView
import OpenSSL.crypto as cert_decoder
from models import PemSubject
from django.db import connection

# Here we don't have main page because we was have in sql_views.py 
# In sql_views.py and orm_views.py i was use same templates.


# Add the certificate template form 
def add_certificate_orm(request):
	return render(request, 'add_certificate_orm.html')


# This function for creating subjects in database 
def create_subject_orm(request):
	pem_text = request.FILES['pem_file']
	open('cert.pem', 'wb').write(pem_text.read())

	# Parsing subject
	st_cert=open('cert.pem', 'rt').read()
	c=cert_decoder
	cert=c.load_certificate(c.FILETYPE_PEM, st_cert)
	subject = cert.get_subject()

	# Create new subject after parsing from certificate
	new_subject = PemSubject.objects.create(**dict(subject.get_components()))
	new_subject.save()

	return redirect('/subject_list_orm')


# This is the CBV for update tables in database
class UpdateSubjectORM(UpdateView):
	model = PemSubject
	fields = ['C', 'ST', 'L', 'O', 'OU', 'CN', 'emailAddress']
	template_name = 'edit_sub.html'
	success_url = '/subject_list_orm'


# This fonction for delete some subjects from database
def delete_subject_orm(request, id):
	PemSubject.objects.get(pk=id).delete()
	return redirect('/subject_list_orm')


# Subjects list from database
def subject_list_orm(request):
	sub_list = PemSubject.objects.all()
	return render(request, 'sub_list.html', {'sub_list': sub_list})





