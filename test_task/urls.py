# -*- coding: utf-8 -*-
"""test_task URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from task.sql_views import *
from task.orm_views import *

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', index),
    # SQL CRUD URL
    url(r'^add_certificate_sql/', add_certificate_sql, name='add_cert_sql'),
    url(r'^subject_list_sql/', subject_list_sql, name='subject_list_sql'),
    url(r'^create_subject_sql/', create_subject_sql, name='add_sql'),
    url(r'^update_subject_sql/(?P<id>\d+)/', update_subject_sql, name='update_sql'),
    url(r'^delete_subject_sql/(?P<id>\d+)/', delete_subject_sql, name='delete_sql'),
    # ORM CRUD URL
    url(r'^add_certificate_orm/', add_certificate_orm, name='add_cert_orm'),
    url(r'^subject_list_orm/', subject_list_orm, name='subject_list_orm'),
    url(r'^create_subject_orm/', create_subject_orm, name='create_subject_orm'),
    url(r'^delete_subject_orm/(?P<id>\d+)/', delete_subject_orm, name='delete_orm'),
    url(r'^update_subject_orm/(?P<pk>\d+)/', UpdateSubjectORM.as_view(), name='update_orm'),
]
